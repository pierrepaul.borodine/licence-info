
#definition constante des différents poids
POIDS_20 = 20
POIDS_100 = 100
POIDS_250 = 250
POIDS_500 = 500
POIDS_1000 = 1000
POIDS_3000 = 3000

#poids de la lettre rentrer par l'utilisateur
poids = float(input("Quel est le poids de votre lettre ? ( en gramme )  \n"))
print("le poids de votre lettre est : ",poids," g")

#type de lettre rentrer par l'utilisateur
type = input("Quel type de lettre voulez-vous envoyer ? (verte, prioritaire ou eco-pli ) \n")
type = type.lower()

#verif type de lettre correct
if type != 'verte' and type != 'prioritaire' and type != 'eco-pli' :
    print("type incorrect")
    verif = False
    while not verif :
        type = input("Quel type de lettre voulez-vous envoyer ? (verte, prioritaire ou eco-pli ) \n")
        verif = True
        if type != 'verte' and type != 'prioritaire' and type != 'eco-pli' :
             verif = False

print("le type de lettre que vous voulez envoyer est : ",type)

#verif si poids trop important selon le type de lettre
if (type == 'verte' and poids > POIDS_3000) or (type =='prioritaire' and poids > POIDS_3000) or (type == 'eco-pli'and poids>POIDS_250):
    print("le poids de votre colis est trop impotant, envoie annulé")

else :

#definition prix selon type de lettre et poids

    if type == 'verte' :
        if poids <= POIDS_20 :
            prix = 1.16
        elif POIDS_20<poids<=POIDS_100:
            prix=2.32
        elif POIDS_100<poids <= POIDS_250:
            prix = 4
        elif POIDS_250<poids<=POIDS_500:
            prix = 6
        elif POIDS_500<poids<=POIDS_1000:
            prix =7.50
        elif POIDS_1000<poids<POIDS_3000 : 
            prix = 10.50

    if type =='prioritaire' :
        if poids <= POIDS_20 :
            prix = 1.43
        elif POIDS_20<poids<=POIDS_100 :
            prix = 2.86
        elif POIDS_100<poids<=POIDS_250 :
            prix = 5.26
        elif POIDS_250<poids<=POIDS_500:
            prix =7.89
        elif POIDS_500<poids<=POIDS_3000: 
            prix = 11.44


    if type =='eco-pli':
        if poids <= POIDS_20 :
            prix = 1.14
        elif POIDS_20<poids<=POIDS_100:
            prix = 2.28
        elif POIDS_100<poids<=POIDS_250 :
            prix = 3.92

#demande ajout du stickers de suivi
    choix = input("Voulez vous un stickers de suivi ? ( O/N ) ")
    choix = choix.upper()
    if choix != 'O' and choix != 'N':
        print("Choix incorrect")
        verif_choix = False
        while not verif_choix :
            choix = input("Voulez vous un stickers de suivi ? ( O/N ) ")
            choix = choix.upper()
            verif_choix = True
            if choix != 'O' and choix != 'N':
                verif_choix = False
    elif choix == 'O':
        print ("ajout d'un sticker au prix initial")
        prix = prix + 0.5
    else:
        print("Pas de stickers de suivi")
#resumé de l'envoie de la lettre
    print("Type de lettre : ",type," , poids de la lettre : ",poids," g, le prix final est donc de ",prix, " euro ")


